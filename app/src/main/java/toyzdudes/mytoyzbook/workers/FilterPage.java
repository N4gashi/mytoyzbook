package toyzdudes.mytoyzbook.workers;

import android.os.Parcel;
import android.os.Parcelable;

public class FilterPage implements Parcelable {

    private String title, constName;
    private int drawable, constValue;

    // region GENERATED STUFF

    public FilterPage(String title, String constName, int drawable, int constValue)
    {
        this.title = title;
        this.constName = constName;
        this.drawable = drawable;
        this.constValue = constValue;
    }

    public FilterPage(Parcel in) {
        this.title = in.readString();
        this.constName = in.readString();
        this.drawable = in.readInt();
        this.constValue = in.readInt();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getConstName() {
        return constName;
    }

    public void setConstName(String constName) {
        this.constName = constName;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public int getConstValue() {
        return constValue;
    }

    public void setConstValue(int constValue) {
        this.constValue = constValue;
    }

    // endregion

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.constName);
        dest.writeInt(this.drawable);
        dest.writeInt(this.constValue);
    }

    public static final Parcelable.Creator<FilterPage> CREATOR = new Parcelable.Creator<FilterPage>()
    {
        @Override
        public FilterPage createFromParcel(Parcel source)
        {
            return new FilterPage(source);
        }

        @Override
        public FilterPage[] newArray(int size)
        {
            return new FilterPage[size];
        }
    };

}
