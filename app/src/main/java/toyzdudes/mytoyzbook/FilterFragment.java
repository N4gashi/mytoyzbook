package toyzdudes.mytoyzbook;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.logging.Filter;


import toyzdudes.mytoyzbook.helpers.DebugHelper;
import toyzdudes.mytoyzbook.workers.FilterPage;

public class FilterFragment extends Fragment {

    private String mTitle;
    private ArrayList<FilterPage> mFilters;
    private NonSwipeableViewPager mViewPager;

    public FilterFragment()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup)inflater.inflate(R.layout.filter_page, container, false);
        Button button = null;
        TextView title = (TextView)rootView.findViewById(R.id.filter_name);
        TextView label = null;

        this.mTitle = getArguments().getString("FILTER_TITLE");
        this.mFilters = getArguments().getParcelableArrayList("FILTER_LIST");
        this.mViewPager = (NonSwipeableViewPager)container;

        for(FilterPage filter : mFilters)
        {

            switch(filter.getConstValue())
            {
                case 1:
                    button = (Button)rootView.findViewById(R.id.first_button);
                    label = (TextView)rootView.findViewById(R.id.first_text);
                    // TODO Bind fonction qui selectionne dans les UserPrefs
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Reset les userpref
                            // Set userpref
                            // nextPage
                            DebugHelper.log('v', "GOD WE MAKE IT THROUGH");
                        }
                    });
                    break;
                case 2:
                    button = (Button)rootView.findViewById(R.id.second_button);
                    label = (TextView)rootView.findViewById(R.id.second_text);
                    // TODO Bind fonction qui selectionne dans les UserPrefs
                    break;
                case 3:
                    button = (Button)rootView.findViewById(R.id.third_button);
                    label = (TextView)rootView.findViewById(R.id.third_text);
                    // TODO Bind fonction qui selectionne dans les UserPrefs et passe au suivant intent
                    break;
                default:
                    break;
            }

            if(title != null)
            {
                Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Pacifico-Light.ttf");
                title.setText(this.mTitle);
                title.setTypeface(typeface);
                title.setTextSize(getResources().getDimension(R.dimen.filter_title_size));
            }

            if (button != null)
                button.setBackgroundResource(filter.getDrawable());

            if(label != null)
                label.setText(filter.getTitle());

        }

        return rootView;
    }

    public static FilterFragment newInstance(int itemPos)
    {
        FilterFragment frag = new FilterFragment();
        Bundle bundle = new Bundle();

        ArrayList<FilterPage> filters = new ArrayList<>();

        switch(itemPos)
        {
            case 0:
                bundle.putString("FILTER_TITLE", "Pour qui ?");
                filters.add(new FilterPage("Pour lui", "FOR_HIM_VALUE", R.drawable.ic_filter_man, 1));
                filters.add(new FilterPage("Pour elle", "FOR_HER_VALUE", R.drawable.ic_filter_woman, 2));
                filters.add(new FilterPage("Pour eux", "FOR_THEM_VALUE", R.drawable.ic_filter_couple, 3));
                break;
            case 1:
                bundle.putString("FILTER_TITLE", "Quelle utilisation ?");
                filters.add(new FilterPage("Point G", "GPOINT_VALUE", R.drawable.ic_filter_pointg, 1));
                filters.add(new FilterPage("Pour elle", "CLITO_VALUE", R.drawable.ic_filter_clito, 2));
                filters.add(new FilterPage("Pour eux", "ANAL_VALUE", R.drawable.ic_filter_anal, 3));
                break;
            case 2:
                bundle.putString("FILTER_TITLE", "Caractéristique principale ?");
                filters.add(new FilterPage("Vibrant", "VIBRATE_VALUE", R.drawable.ic_filter_vibrating, 1));
                filters.add(new FilterPage("Silicone", "SILICONE_VALUE", R.drawable.ic_filter_silicon, 2));
                filters.add(new FilterPage("Waterproof", "WATERPROOF_VALUE", R.drawable.ic_filter_waterproof, 3));
                break;
            default:
                break;
        }


        bundle.putParcelableArrayList("FILTER_LIST", filters);
        frag.setArguments(bundle);

        return frag;
    }
}
